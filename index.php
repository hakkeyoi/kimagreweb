<?php get_header();?>



<div class="container">
    <div class="inner">
        <ul class="column" id="read-more">

        <?php if(have_posts()):while(have_posts()):the_post();?>
        <li class="item wow bounceInUp"><article class="index-article">
            <a href="<?php the_permalink();?>">
                <h2 class="title-label"><?php the_title();?></h2>
                <p class="date-label"><?php the_date("Y年n月j日 l"); ?></p>
                <p class="category-label"><i class="fas fa-archive"></i><?php the_category(', ');?></p>
                <p class="tag-label">
                    <i class="fas fa-tags"></i>
                    <?php the_tags('',', ');?>
                </p>
                <p class="excerpt"><?php echo get_the_excerpt(); ?></p>
            </a>
        </article>
        </li>

        <?php endwhile; //while loop終了 ?>

        <?php else: echo "投稿なし"; endif; ?>
        </ul>
        <div class="pagenavi">
            <?php posts_nav_link(); ?>
        </div>
        
	
       

    </div>
</div><!--container-->

<?php get_footer();?>
