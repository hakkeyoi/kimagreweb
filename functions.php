<?php


//デフォルトのjqueryやcssを読み込まない関数
function delete_local_scripts() {
    wp_deregister_script('jquery');
    wp_deregister_script( 'dashicons' );
    wp_dequeue_style('wp-pagenavi');
    wp_enqueue_style('style', get_template_directory_uri()."/style.css",array('normalize','animate','highlightcss'),date('YmdGi', filemtime(get_template_directory().'/style.css')));
    wp_enqueue_style('animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css');
    wp_enqueue_style('highlightcss', 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css');
    wp_enqueue_style('normalize', get_template_directory_uri().'/css/normalize.css','');
    wp_enqueue_script('jquery3',"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");
    wp_enqueue_script('tile', get_template_directory_uri().'/js/jquery.tile.js','');
    wp_enqueue_script('inview', get_template_directory_uri().'/js/popup.js');
    // wp_enqueue_script('wow', 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js','');
    wp_enqueue_script('highlightjs', '//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js','');
    wp_enqueue_script('functions', get_template_directory_uri().'/js/functions.js?1fafdsas45','');

}
//上記実行
add_action( 'wp_enqueue_scripts', 'delete_local_scripts' );

//ほか余計なもの読み込まない。
remove_action('wp_head','rsd_link' );
remove_action('wp_head','wlwmanifest_link' );
remove_action('wp_head','wp_generator' );
remove_action('wp_head','wp_shortlink_wp_head' );
remove_action('wp_head','rest_output_link_wp_head' );
remove_action('wp_head','wp_oembed_add_discovery_links' );
remove_action('wp_head','wp_oembed_add_host_js' );
remove_action('wp_head','print_emoji_detection_script', 7 );
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rel_canonical');

add_theme_support( 'title-tag' );

//タイトル出力
add_theme_support('title-tag','');

function custom_theme_setup() {
    add_theme_support('title-tag');
}

add_action( 'after_setup_theme', 'custom_theme_setup' );

add_theme_support( 'title-tag' );

add_filter( 'document_title_separator', 'my_document_title_separator' );
function my_document_title_separator( $sep ) {
  $sep = '｜';
  return $sep;
}


//カスタムメニュー
///add_theme_support('menus');

function menu_setup() {
	
	register_nav_menus( array(
		'top-nav' => 'トップナビ',
    ) );
    
    register_nav_menus( array(
		'footer-nav' => 'フッターナビ',
	) );
}

add_action( 'after_setup_theme', 'menu_setup' );



//ajaxで検索するための関数

function ajaxSearchPost(){
    if($_POST['searchWord'] != ''){
        $s = array(
            's' => $_POST['searchWord'],
        );
        $query = new WP_Query($s);
        $result;
        if ( $query->have_posts() ) :
            while ( $query->have_posts() ) {
                $query->the_post();
                $result .= '<li><a href="'.get_the_permalink().'">' . get_the_title() . '</a></li>';
            }
        endif;
    echo $result;
    wp_reset_postdata();
    }
    //$_POST['searchWord'];
    die();
}


add_action( "wp_ajax_ajaxSearchPost" , "ajaxSearchPost" );
add_action( "wp_ajax_nopriv_ajaxSearchPost" , "ajaxSearchPost" );

function add_search_ajax(){
?>

<script >
  var search_ajax_url  = '<?php echo admin_url('admin-ajax.php', __FILE__); ?>';
</script>

<?php
}
add_action('wp_head', 'add_search_ajax',1); 


/* テーマカスタマイザー
---------------------------------------------------------- */
add_action( 'customize_register', 'theme_customize' );

function theme_customize($wp_customize){
//ロゴ画像
$wp_customize->add_section( 'logo_section', array(
'title' => 'ロゴ画像', //セクションのタイトル
'priority' => 59, //セクションの位置
'description' => 'ロゴ画像を使用する場合はアップロードしてください。画像を使用しない場合はタイトルがテキストで表示されます。', //セクションの説明
));
$wp_customize->add_setting( 'logo_url' );
$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_url', array(
'label' => 'ロゴ画像',//セッティングのタイトル
'section' => 'logo_section', //セクションID
'settings' => 'logo_url', //セッティングID
'description' => 'ロゴ画像を設定してください。', //セッティングの説明
)));
}

/* テーマカスタマイザーで設定された画像のURLを取得
---------------------------------------------------------- */
//ロゴ画像
function get_the_logo_url(){
    return esc_url( get_theme_mod( 'logo_url' ) );
    }