
$(function(){
    //menuの開閉
    $('.menu-btn').on('click',function(){
        $('.main-nav').fadeIn(800).addClass('show');//.toggleClass('show',5000);
    });

    $('#menuClose').on('click',function(){
        $('.main-nav').fadeIn(800).removeClass('show');
    });


    $('#searchClose').on('click',function(){
        location.reload();
    });

    //highlight.jsを実行
    hljs.initHighlightingOnLoad();
});


$(window).on('load',function(){
    //columnの高さ揃え。loadイベントでないと正しいheightが取得できない。
    adjustHeight();
});

hljs.initHighlightingOnLoad();
$(window).on('resize',function(){ 
    //リサイズするたびにcolumnの高さを調整しなおす。
    adjustHeight();

    if($(window).outerWidth() > 768){
        //$('.main-nav').fadeIn(800).removeClass('show');
    }
});

//カラムの高さを調整
function adjustHeight(){
    let w = $(window).outerWidth();
    let column = $('.column');
    if(w <= 579.99 ){
        column.children('li').css('height','auto');
    }else if(579.99 < w && w < 768.9 ){
        column.children('li').tile(2);
    }else{
        column.children('li').tile(3);
        
    }
}


$(function(){

    function changeColor(gradation,fontColor){
        $('.header-nav').css('background',gradation);
        $('.home-category-title').css('background',gradation);
        $('.searchResultBox').css('color',fontColor);
        $('.footer').css('background',gradation);
    }

    function changeGreen(){
        var fontColor = '#00904a';
        var gradation = 'linear-gradient(to right, rgba(107, 183, 86, 0.95), rgba(0, 143, 104, 0.95))';
        changeColor(gradation,fontColor);
    }

    function changeOrange(){
        var fontColor = 'orange'
        var gradation = 'orange';
        changeColor(gradation,fontColor);
    }

    function changeBlue(){
        let fontColor = 'orange'
        let gradation = 'orange';
        changeColor(gradation,fontColor);
    }


    $('#theme-color').on('change',function(){
        var name = $(this).val();
        console.log(name);
        
        switch (name){
            case 'green':
                changeGreen();
                break;

            case 'orange':
                changeOrange();
                break;

            case 'orange':
                changeBlue();
                break;

        }

        
    });
});