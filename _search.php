<?php get_header(); ?>
 
<?php
    global $wp_query;
    $total_results = $wp_query->found_posts;
    $search_query = get_search_query();
?>

<h1><?php echo $search_query; ?>の検索結果<span>（<?php echo $total_results; ?>件）</span></h1>
 
<?php
    //文字1件以上ヒットしたら
    if( $total_results >0 ):
    if(have_posts()):
    while(have_posts()): the_post();
?>
 
<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
<?php the_excerpt(); ?>
 
<?php endwhile; endif; else: ?>
 
<?php echo $search_query; ?> に一致する情報は見つかりませんでした。
 
<?php endif; ?>