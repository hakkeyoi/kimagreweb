<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/earlyaccess/kokoro.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Life+Savers:400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,700" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body>

<header class="header">
	<div class="header-nav">
		<div class="header-inner">
			<?php if ( !is_front_page() && !is_home() ): ?>
				<a class="site-logo" href="/">
					<?php $logo_url = get_the_logo_url('logo_url'); ?>
					<?php if($logo_url): ?> 
						<img src="<?php echo get_the_logo_url(); ?>">
					<?php else: ?>
						<img src="/wp-content/themes/kimagreweb/images/panda.png">
					<?php endif; ?>
				</a>
			<?php endif;?>


			<!-- topのメインメニュー -->
			<nav class="main-nav">
				<div class="menuCloseWrap">
					<div id="menuClose" class="menuClose"><i class="fas fa-times"></i>Close</div>
				</div>
				<?php
				$nav= array(
					'menu'            => '',
					'menu_class'      => 'menu',
					'menu_id'         => 'top-nav',
					'container'       => 'div',
					'container_class' => 'top-menu',
					'container_id'    => 'pc-menu',
					'fallback_cb'     => 'wp_page_menu',
					// 'before'          => '',
					// 'after'           => '',
					// 'link_before'     => '',
					// 'link_after'      => '',
					'echo'            => true,
					'depth'           => 0,
					'walker'          => '',
					'theme_location'  => 'top-nav',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				);
				wp_nav_menu( $nav );
				?> 
			</nav>

			<div class="">
				<select id="theme-color" name="theme-color">
					<option value="green">Green</option>
					<option value="orange">Orange</option>
					<option value="blue">blue</option>
				</select>
			</div>

			<div class="top-nav-icon">
				<div class="searchWindow">
					<input type="text" id="searchInput" value="" name="s" />
				</div>
				<a class="menu-btn" href="#"><i class="fas fa-ellipsis-h"></i></a>
			</div>
		</div>
	</div>
	<?php if ( is_front_page() && is_home() ): ?>
		<div class="home-category-title">
			<div class="logo">
			
				<?php $logo_url = get_the_logo_url('logo_url'); ?>
				<?php if($logo_url): ?> 
					<img src="<?php echo get_the_logo_url(); ?>">
				<?php else: ?>
					<img src="/wp-content/themes/kimagreweb/images/panda.png">
				<?php endif; ?>
			
			</div>
			<h1><?php bloginfo('name'); ?></h1>
			<p><?php bloginfo('description'); ?></p>
		</div>

	<?php elseif (is_category() ): ?>

		<div class="home-category-title">
			<h1><?php $category = get_category($cat);echo $category->name;?></h1>
			<p><?php echo category_description(); ?></p>
		</div>

	<?php elseif (is_single() ): ?>

		<div class="home-category-title">
			<h1><?php the_title();?></h1>
			<p><?php echo category_description(); ?></p>
		</div>

	<?php elseif (is_page() ): ?>

		<div class="home-category-title">
			<h1><?php the_title();?></h1>
			<p><?php echo category_description(); ?></p>
		</div>

	<?php else: ?>

		<div class="home-category-title">
			<h1>Other</h1>
			<p><?php //echo category_description(); ?></p>
		</div>

	<?php endif;?>

</header>



<script>
	$('#searchInput').on('keyup',function(){
		$.ajax({
			type:'POST',
			url:search_ajax_url,
			data:{
				'searchWord':$('#searchInput').val(),
				action:'ajaxSearchPost',
			},
			success: function(data){
				$('.home-category-title').addClass('hide');
				$('.main-content').addClass('hide');
				$('.searchResultBox').addClass('show');
				$('#searchResult').html(data);
			},
		});
		return false;
	});
</script>


<script>



</script>


<main>

<div class="searchResultBox">
	<div id="searchClose" class="searchClose"><i class="fas fa-times"></i>Clear</div>
	<h2 class=""><i class="fas fa-smile"></i>Search Results</h2>
	<ul id="searchResult"></ul>
</div>





