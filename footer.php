<footer class="footer">
<?php
$nav= array(
    'menu'            => '',
    'menu_class'      => 'menu',
    'menu_id'         => 'footer-nav',
    'container'       => 'div',
    'container_class' => 'footer-menu',
    'container_id'    => '',
    'fallback_cb'     => 'wp_page_menu',
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'echo'            => true,
    'depth'           => 0,
    'walker'          => '',
    'theme_location'  => 'footer-nav',
    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
);
wp_nav_menu( $nav );
?> 

</footer>
<?php wp_footer();?>
</body>
</html>