<?php get_header();?>



<main id="read-more" class="container">
    <div class="inner">
        <article class="index-article">
        <?php if(have_posts()):while(have_posts()):the_post();?>
        
            <h2 class="title-label"><?php the_title();?></h2>
            
            <div class="content">
                <div class="flex-left">
                    <p class="date-label"><?php the_date("Y年n月j日 l"); ?></p>
                    <p class="category-label"><i class="fas fa-archive"></i><?php the_category(', ');?></p>
                    <p class="tag-label">
                        <i class="fas fa-tags"></i>
                        <?php the_tags('',',');?>
                    </p>
                </div>
                <?php echo the_content(); ?>
            </div>
    
        <?php endwhile; //while loop終了 ?>

        <?php else: echo "投稿なし"; endif; ?>
        </article>

    </div>
</main>
<?php get_footer();?>
